package jogodavelha;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class JogoDaVelha extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent front = FXMLLoader.load(getClass().getResource("Principal.fxml"));
        
        Scene scene = new Scene(front);
        stage.setTitle("Jogo da Velha com Minimax");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
}

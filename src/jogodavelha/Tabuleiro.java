package jogodavelha;

import java.util.ArrayList;

public class Tabuleiro {

    private String[][] grade = new String[3][3];
    private Jogador jogador;
    private int valor = 0;

    private ArrayList<Tabuleiro> filhos = new ArrayList<Tabuleiro>();
    
    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public Jogador getJogador() {
        return jogador;
    }

    public void setJogador(Jogador jogador) {
        this.jogador = jogador;
    }

    public String getXY(int linha, int coluna) {
        return grade[linha][coluna];
    }

    public String[][] getMapa() {
        return grade;
    }

    public void setX(int linha, int coluna) {
        grade[linha][coluna] = "X";
    }

    public void setO(int linha, int coluna) {
        grade[linha][coluna] = "O";
    }

    private boolean verificaHorizontal(int linha) {
        return ("O".equals(grade[linha][0]) && "O".equals(grade[linha][1]) && "O".equals(grade[linha][2]));
    }

    private boolean verificaVertical(int coluna) {
        return ("O".equals(grade[0][coluna]) && "O".equals(grade[1][coluna]) && "O".equals(grade[2][coluna]));
    }

    private boolean verificaDiagonalPrincipal() {
        return ("O".equals(grade[0][0]) && "O".equals(grade[1][1]) && "O".equals(grade[2][2]));
    }

    private boolean verificaDiagonalSecundaria() {
        return ("O".equals(grade[0][2]) && "O".equals(grade[1][1]) && "O".equals(grade[2][0]));
    }

    private boolean verificaPerdeuHorizontal(int linha) {
        return ("X".equals(grade[linha][0]) && "X".equals(grade[linha][1]) && "X".equals(grade[linha][2]));
    }

    private boolean verificaPerdeuVertical(int coluna) {
        return ("X".equals(grade[0][coluna]) && "X".equals(grade[1][coluna]) && "X".equals(grade[2][coluna]));
    }

    private boolean verificaPerdeuDiagonalPrincipal() {
        return ("X".equals(grade[0][0]) && "X".equals(grade[1][1]) && "X".equals(grade[2][2]));
    }

    private boolean verificaPerdeuDiagonalSecundaria() {
        return ("X".equals(grade[0][2]) && "X".equals(grade[1][1]) && "X".equals(grade[2][0]));
    }

    /**
     * Verifica se houve empate
     * @return 
     */
    public boolean empate() {
        boolean empate = true;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (grade[i][j] == null) {
                    empate = false;
                    break;
                }
            }
        }

        return empate && !ganhou();
    }

    /**
     * Verifica se o Computador Perdeu
     * @return 
     */
    public boolean perdeu() {
        return (verificaPerdeuHorizontal(0)
                || verificaPerdeuHorizontal(1)
                || verificaPerdeuHorizontal(2)
                || verificaPerdeuVertical(0)
                || verificaPerdeuVertical(1)
                || verificaPerdeuVertical(2)
                || verificaPerdeuDiagonalPrincipal()
                || verificaPerdeuDiagonalSecundaria());
    }

    /**
     * Verifica se o computador ganhou
     * @return 
     */
    public boolean ganhou() {
        //return false;
        return (verificaHorizontal(0)
                || verificaHorizontal(1)
                || verificaHorizontal(2)
                || verificaVertical(0)
                || verificaVertical(1)
                || verificaVertical(2)
                || verificaDiagonalPrincipal()
                || verificaDiagonalSecundaria());
    }

    /**
     * Verifica se ele é um nó folha
     * @return TRUE caso for folha
     * FALSE caso não for folha
     */
    public boolean isTerminal() {
        return ganhou() || empate() || perdeu();
    }

    /**
     * 
     * @return (int) o resultado de acordo com o tabuleiro, se o computador Ganhou(1), Perdeu(-1) ou Empatou(0).
     */
    public int getResultado() {
        if (ganhou()) {
            valor=1;
            return 1;
        } else {
            if (perdeu()) {
                valor=-1;
                return -1;
            } else {
                return 0;
            }
        }
    }

    /**
     * Mostra o tabuleiro formatado no Console.
     */
    public void mostra() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(grade[i][j] + "\t");
            }
            System.out.println("");
        }
        System.out.println("Utilidade: "+this.getValor());
    }

    /**
     * Só vai pegar a lista dos filhos
     * Para descobrir quem são os filhos usar o getFilhos()
     * @see getFilhos(Tabuleiro t)
     * 
     * @return 
     */
    public ArrayList<Tabuleiro> getTodosFilhos(){
        return filhos;
    }
    
    /**
     * Vai buscar todas as possibilidades (Filhos) do nó
     * @param t
     * @return 
     */
    public ArrayList<Tabuleiro> getFilhos(Tabuleiro t) {
        filhos = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                try {
                    Tabuleiro filho = t.clone();
                    if (filho.getXY(i, j) == null) {
                        if (jogador.equals(Jogador.Max)) {
                            filho.setO(i, j);
                        } else {
                            filho.setX(i, j);
                        }
                        filho.getResultado();
                        filhos.add(filho);
                    }
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        }
        return filhos;
    }

    @Override
    protected Tabuleiro clone() throws CloneNotSupportedException {
        Tabuleiro t = new Tabuleiro();
        for (int i = 0; i < 3; i++) {
            System.arraycopy(grade[i], 0, t.getMapa()[i], 0, 3);
        }
        return t;
    }

    @Override
    public String toString() {
        String s = "[";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                s+=grade[i][j]+",";
            }
        }
        return s+"]\n";
    }

}
